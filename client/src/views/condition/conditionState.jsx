import React, { Component } from "react";

class ClassCondition extends Component {
  //đây là ta dùng state để condition
  state = {
    showSanpham : true
  }
  handldeShowhide = () => {
    this.setState({
      showSanpham : !this.state.showSanpham //! nghĩa là phủ định,đã học ở js rồi
    })
  }
   render () {
     let { Sanpham2 } = this.props;
     let { showSanpham } = this.state;
     console.log(showSanpham);
      return (
        <>
        {
          showSanpham === false &&  
          <div>
            <button onClick={() => {this.handldeShowhide()} }>Show</button>
          </div>
        }
        {
          showSanpham && 
          <>
           <div className="arrconditon">
            {
            Sanpham2.map((item) => {
              return (
               
                <div key={item.id}>
                  {item.name} - {item.price}
                </div>
              )
            })
           }
          </div>
           <div>
              <button onClick={() => {this.handldeShowhide()} }>Hide</button>
            </div>
          </>
        }  
        
        </>
         
      )
   }

}
export default ClassCondition;