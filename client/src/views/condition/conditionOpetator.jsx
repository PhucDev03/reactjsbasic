import React, { Component } from "react";
/**
 * ví dụ này không khác gì conditionState chỉ là thay vì sử dụng câu điều kiện && 
 * thì ta sử dụng câu điều kiện toán tử 3 ngôi để gọn code hơn và thực tế hơn
 */
class ClassConditionOpetartor extends Component {
 
  state = {
    showSanpham : true
  }
  handldeShowhide = () => {
    this.setState({
      showSanpham : !this.state.showSanpham 
    })
  }
   render () {
     let { Sanpham2 } = this.props;
     let { showSanpham } = this.state;
     console.log(showSanpham);
      return (
        //cấu trúc là {condition ? <componentA/> : <componentB/>} gọn hơn rất nhiều so với sử dụng &&
        <>
        {
          showSanpham === false ?
          <div>
            <button onClick={() => {this.handldeShowhide()} }>Show</button>
          </div>
          : 
          <>
           <div className="arrconditon">
               {
            Sanpham2.map((item) => {
              return (
               
                <div key={item.id}>
                  {item.name} - {item.price}
                </div>
              )
            }) 
              }
           </div>
           <div>
              <button onClick={() => {this.handldeShowhide()} }>Hide</button>
            </div>
          </>
        }
        </>
      )
   }

}
export default ClassConditionOpetartor;