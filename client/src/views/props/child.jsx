import React,{ Component } from "react";

class ClassChild extends Component {
  state = {
    name : "",
    price : ""
  }
  //[14.2]ở đây obj chính là item ở phía dưới mà ta đã truyền vào từ việc lọc mảng. 
  handleOnClick = (obj) => {
    this.props.deleteAJob(obj)
  };
  render() {
  
     //this.props sẽ kiểm tra props mà thằng cha truyền xuống cho nó.
      //cách lấy đầu tiên  {this.props.name},nó hơi thô ta đặt biến cho nó đỡ hơn.
    

    let testname = this.props.name;
    let testage = this.props.age;
    /**
     * đây là cú pháp ngắn gọn.Ta hiểu đại khái là,khi tên biến ta muốn đặt nó lại trùng với tên key của props mà thằng cha nó truyền thì ta sẽ sử dụng
     * Lưu ý là tên biến và key trùng nhau(và có ý nghĩa,chứ không cố tình đặt cho trùng) thì mới sử dụng.
     *  */ 
    let {name,age,Sanpham} = this.props;
    console.log(this.props);
    return (
     
      <>
        <div>name: {this.props.name}</div>
        <div>name: {testname} </div>
        <div>age : {testage}</div>
        <div>all:{name} - {age}</div>
        <div className="arr">
          {
            Sanpham.map((item) => {
              return (
                <div key={item.id}>
                  {item.name} - {item.price} <span  onClick={() => this.handleOnClick(item)}>X</span> 
                </div>
              )
            })
          }
        </div>
      </>
    )
  }
}
export default ClassChild;