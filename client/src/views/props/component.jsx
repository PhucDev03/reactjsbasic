import React,{ Component } from "react";
import ClassChild from "./child";
import FuncStateLess from "../stateless/stateless";
import ClassCondition from "../condition/conditionState";
import ClassConditionOpetartor from "../condition/conditionOpetator";
import ClassForm from "../../components/form";
class Classfather extends Component {
  //ta đã làm sơ qua việc truyền trực tiếp,biết các cú pháp cơ bản.Tuy nhiên nếu có nhiều dữ liệu của thằng cha mà nó liên tục thay đổi và nó muốn truyền cho thằng con thì làm sao.Ta sẽ dùng State =))
  state = {
    Sanpham : [
      {id : "sp1",name : "BỘT GIẶT",price : "500"},
      {id : "sp2",name : "BỘT NGỌT",price : "600"},
      {id : "sp3",name : "BỘT NGÔ",price : "700"}
    ],
    Sanpham2 : [
      {id : "sp12",name : "BỘT GIẶT",price : "800"},
      {id : "sp22",name : "BỘT NGỌT",price : "900"},
      {id : "sp32",name : "BỘT NGÔ",price : "1000"}
    ]
  }
  //đầu tiên ta phải tạo một function để nhận dữ liệu từ thằng con,lý do tại sao không dùng state như mọi khi là vì nó sẽ không bao giờ nhận dữ liệu từ thằng con truyền vào(học rồi).Nên mới sử dụng kiểu function này
  addNewJob = (obj) => {
    console.log("check data",obj);
    this.setState({
      /**
       * Ta giải thích một chút về đoạn code này
       * -Đầu tiên là Sanpham2.Nó là key dữ liệu của state mà ta trỏ tới,nghĩa là trong trường hợp này 
       * ta đang muốn setState cho Sanpham2.
       * -Tiếp : sau dấu hai chấm ấy thì chính là dữ liệu mà ta gán vào key đó gọi là value.Học hết rồi,từ từ mà suy nghĩ rồi hiểu thôi.
       * - [] cái này chính là ta đang muốn tạo ra một mảng mới,... này nó có nghĩa là sao chép mà sao chép cái gì thì đây
       * this.state.Sanpham2 nó sẽ sao chép tất cả các item của mảng này,nghĩa là bây giờ ta đã có một mảng mới và sao chép hết item của mảng mà ta đã tham chiếu.
       * -Cuối cùng là obj,phần này ta chỉ cần biết là nó sẽ thêm một obj mới do thằng con truyền vào là được.Dấu , ta hiểu là tách các obj khác ra ấy.Nhìn obj sanpham trên state là thấy
       * -sau khi ta truyền vô rồi thì thằng setState sẽ xem xét và nhận thấy rằng sanpham2 đã có sự thay đổi,cụ thể là nó thêm một obj mới cho nên nó sẽ cập nhật lại biến state và nó sẽ thêm 
       * obj mà ta mới vừa truyền cho nó.Lúc này state của ta đã chính thức nhận được obj mà ta truyền từ thằng con -> thằng cha.
       * 
      */
      Sanpham2 : [...this.state.Sanpham2, obj] 
    })
    
  }
  //ví dụ xóa đây
  deleteAJob = (obj) =>{
    //ta tham chiếu tới mảng cần chỉnh sửa
    let currenJobs = this.state.Sanpham;
    //ta dùng hàm filter để lọc ra,ở đây ta đang lọc ra những obj nào khác với id obj mà chúng ta cần xóa thì giữ lại.còn giống thì xóa.
    currenJobs = currenJobs.filter(item => item.id !== obj.id);
    //cuối cùng ta sử dụng setstate để cập nhật thôi
    this.setState({
      Sanpham : currenJobs
    })
  }
  
  render() {
   
    return (
      <>
      <div>Component</div>
      <ClassForm
      //ta truyền function as props,nên nhớ props có dạng là key và value.Và addNewJob này nói trắng ra nó chính là  (obj) => {}.:D ok chưa
        addNewJob = {this.addNewJob}
      />
      {/* <form>
            <label htmlFor="FrName">
              Tên Sản phẩm:
              <input type="text" name="name" value={this.state.Sanpham.name} 
                 onChange={(event) => this.handleOnChangeFName(event)}
              />
            </label>
            <label htmlFor="FrValue"/>
              Gía Tiền:
            <input type="text" value={this.state.Sanpham.price} 
                  onChange={(event) => this.handleOnChangeFValue(event)}
            />
      </form> */}
      <ClassChild
        //Đơn giản không,nếu ta đã tiếp xúc thao tác rồi,thì việc truyền prop này nó y chang việc ta truyền cho các thẻ khác như hôm trước vậy.Tuy nhiên đây chỉ là đơn giản.
        //Trong thực tế ta thường truyền data bằng obj hay arr bởi vì thực tế data sẽ có rất rất nhiều.
        name = {this.state.name}
        age = {this.state.value}
       Sanpham = {this.state.Sanpham}
       deleteAJob={this.deleteAJob}
      />
       <FuncStateLess
        Sanpham2 = {this.state.Sanpham2}
       />
       <ClassCondition 
        Sanpham2 = {this.state.Sanpham2}
       />
       <ClassConditionOpetartor
        Sanpham2 = {this.state.Sanpham2}
       />
      </>
     
      
    )
  }
}
export default Classfather;