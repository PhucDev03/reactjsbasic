//Nó thường được viết dưới dạng function componet
//Nó chỉ sử dụng khi component nhận prop từ cha và render ra dữ liệu và không quan tâm state thì dùng.
//Đặc điểm đặc biệt của function component là lấy dữ liệu không cần dùng this.Thay vào đó ta sẽ nhận biến props theo kểu được truyền vào(na ná đối tượng event):DD 
import React from "react";
//ta viết arrowfunction
const FuncStateLess = (props) =>{
    let { Sanpham2 } = props;
   
  return (
    <>
     <div className="arr">
          {
            //Đây là ta đang sử dụng map để lọc dữ liệu từ mảng được truyền vào từ thằng cha.
            Sanpham2.map((item) => {
              return (
                <div key={item.id}>
                  {item.name} - {item.price} 
                
                </div>
              )
            })
          }
        </div>
    </>
   
  )
}
export default FuncStateLess;