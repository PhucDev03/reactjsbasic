//ví dụ về functional component
//câu lệnh này nhằm mục đích nhúng module react cụ thể hơn là Component.Vào thư mục này để sài các tính năng của react,ở đây ta là ta đang sử dụng một trình biên dịch của react để biên dịch mã jsx
import React, { Component } from "react";
import ClassForm from "../../components/form";
import "./text.scss";
//extends có nghĩa là kế thừa,ở đây class tên ClassComponent đã kế thừa từ đối tượng component nên ClassComponent mới có thể sử dụng các tính năng của thằng cha nó.
class ClassComponent extends Component {
  //trạng thái ban đầu của component
  state = {
    stateDoc: {
      title: "Bài số 1",
      text: "Nội dung bài số 1",
    },
    stateAcc: {
      name: "phuc",
      age: 20,
    }, 
    stateFrom : {
      name : "",
      value : ""
    }
  };
  handleOnChangeTitle = (event) => {
    //thay đổi,tạm dịch qua tiếng việt nghĩa là cài lại trạng thái.This.setstate là trỏ tới state mà ta đã khai báo trước đó
    this.setState({
      stateDoc: {
        title: event.target.value, //giá trị thay đổi mà ta cập nhật
        //ta phải truyền đầy đủ bởi vì 2 giá trị điều luôn cập nhật
        text: this.state.stateDoc.text,
      },
    });
  };
  handleOnChangetext = (event) => {
    this.setState({
      stateDoc: {
        //ta phải truyền đầy đủ bởi vì 2 giá trị điều luôn cập nhật
        title: this.state.stateDoc.title,
        text: event.target.value, //giá trị thay đổi mà ta cập nhật
      },
    });
  };
  handleOnChangeName = (event) => {
    this.setState({
      stateAcc: {
        name: event.target.value,
        age: this.state.stateAcc.age,
      },
    });
  };
  handleOnChangeFName = (event) =>{
    this.setState({
      stateFrom : {
        name : event.target.value,
        value : this.state.stateFrom.value
      }
    })
  }
  handleOnChangeFValue = (event) =>{
    this.setState({
      stateFrom : {
        name : this.state.stateFrom.name,
        value : event.target.value
      }
    })
  }
  handleOnFClick = (event) =>{
    event.preventDefault();
    alert(this.state.stateFrom.name.text)
  }
  handleOnClick = () => {
    alert("xin chào");
  };
  //đây là phương thức render của component và ClassComponent được kế thừa và sử dụng
  render() {
    //bản chất đây vẫn là js nên ta hoàn toàn có thể viết code js ở đây và viết được cả js ở trong html,khi dùng ta phải {}.
    return (
      //giá trị gán là html,đây là đặc điểm của jsx
      <>
        <div>
          <div className="Cre">
            <h2>Tạo Bài Viết</h2>
            <h3>Tiêu đề bài</h3>
            <input
              value={this.state.stateDoc.title}
              type="text"
              onChange={(event) => this.handleOnChangeTitle(event)}
            />

            <h3>Nội Dung bài</h3>
            <textarea
              cols="46"
              rows="3"
              name="comments"
              value={this.state.stateDoc.text}
              type="text"
              onChange={(event) => this.handleOnChangetext(event)}
            ></textarea>
            <h3>Tên Tác Gỉa</h3>
            <input
              value={this.state.stateAcc.name}
              type="text"
              onChange={(event) => this.handleOnChangeName(event)}
            />
            <div className="Doc">
              <h1>Kết quả bài viết</h1>
              {console.log(this.state.stateDoc)}
              <h4>{this.state.stateDoc.title}</h4>
              <p>{this.state.stateDoc.text}</p>
              <h3>{this.state.stateAcc.name}</h3>
              <h3>tuổi {this.state.stateAcc.age}</h3>
            </div>
          </div>
          <form>
            <label htmlFor="FrName">
              Name:
              <input type="text" name="name" value={this.state.stateFrom.name} 
                 onChange={(event) => this.handleOnChangeFName(event)}
              />
            </label>
            <label htmlFor="FrValue">
              value:
            <input type="text" value={this.state.stateFrom.value} 
                  onChange={(event) => this.handleOnChangeFValue(event)}
            />
            </label>
            <input type="submit" 
                   onClick={(event) => this.handleOnFClick(event)}
            />
          </form>

          {/* <h1 className="name">tên {this.state.keya.name}</h1>
          <h1 className="age">tuổi {this.state.keya.age}</h1>
          <h1 className="name">tên {this.state.keyb.name}</h1>
          <h1 className="age">tuổi {this.state.keyb.age}</h1> */}
        </div>
        <div>
          <button onClick={() => this.handleOnClick()}>click me</button>
        </div>
        <ClassForm/>
      </>
    );
  }
}

//export dịch nghĩa là xuất và mục đích y như cái tên của nó.Ta muốn clascomponent được sử dụng ở nơi khác thì ta phải cho nó xuất ra.
//default dịch nghĩa là mặc định,thì ta hiểu nôm na là chỉ có một thành phần trong file thôi nên ta để default cho react biết là "xuất một thành phần duy nhất" trong file.
//nếu ta có nhiều thành phần hơn thì ta làm như sau
//export { ClassComponent, ClassComponent2 }; hay export [ClassComponent, ClassComponent2];
//hoặc ta sử dụng câu lệnh export ngay khi khai báo class: export class ClassComponent extends Component {};
export default ClassComponent;
//===============================================================================================
//ví dụ về functional component
//câu lệnh này giống câu lệnh trên nhưng nó sẽ nhúng tất cả của react vào.
// import React from 'react';
// //ta chỉ cần khai báo hàm là được,ở đây ta dùng arrow function nên cần khai báo biến để lưu hàm này.
// const MyComponent = () => {
//   return (
//     <div>
//       <h1>Hello from My Component!</h1>
//     </div>
//   );
// };

//export default MyComponent;
//================================================================================================================================
/**
 * ngoài ra còn rất nhiều kiến thức về component mà ta không thể kể hết được,ta chỉ mới làm quen cách sử dụng nó thôi,còn lại tự gg.
 */
