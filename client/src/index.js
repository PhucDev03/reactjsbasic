/**
 * file này cực kỳ quan trọng,nó là file kết nối với index.html(trang duy nhất của ta).Cho nên tất cả các thành phần của web
 * điều tập trung hết ở đây.
 */
//ta nhúng các thư viện,thành phần như file scss,và bây giờ ta nhúng thành phần ở bên components để demo
import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './views/App';
import './style/global.scss';
import reportWebVitals from './reportWebVitals'; //kiểm tra hiệu năng 
import ClassComponent from './views/example/class';

//mặc dù chưa học nhưng ta đọc là hiểu,đây là câu lệnh tạo ra root.
const root = ReactDOM.createRoot(document.getElementById('root'));
//nơi mà ta sẽ render(hiểu nôm na là xây và xuất ra) các thành phần để tạo nên website
root.render(
  <React.StrictMode>
    <App />
    <ClassComponent />
  
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
