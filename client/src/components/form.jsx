import React, { Component } from "react";

class ClassForm extends Component {
  //nên nhớ state luôn luôn là nơi chứa và lưu trữ các trạng thái của component,và ta có thể dùng nó để thao tác.
  state = {
    name : "",
    price : ""
  }

  handleOnChangeFName = (event) =>{
    this.setState({
      name : event.target.value,
      price : this.state.value
    })
  }
  handleOnChangeFValue = (event) =>{
    this.setState({
      name : this.state.name,
      price : event.target.value
    })
  }
  //[14.1]
  handleOnFClick = (event) =>{
    event.preventDefault();
    //check xem người dùng đã nhập chưa
    if (!this.state.name || !this.state.price) {
      alert("lỗi bạn chưa nhập đầy đủ")
      return;
    }
    console.log("check data",this.state);
    //ta gọi đối tượng props và gọi tới key mà thằng cha truyền vào.Và dữ liệu của ta là một obj thế nên ta sẽ sử dụng {key:value},ez thôi.
    this.props.addNewJob({
      id : Math.floor(Math.random() * 100),//đây là câu lệnh random số,trong đó floor nó sẽ làm tròn,còn random() * số,là chúng ta sẽ cho nó random bất kỳ trong khoản từ 0 -> số mà ta muốn(ở đây là 100).
      name : this.state.name,
      price : this.state.price
    })
    //đây là sau khi thêm rồi thì ta cần xóa đi những cái còn tồn đọng trong input.Ta chỉ cần set lại thôi
    this.setState(
      {
        name : "",
        price : ""
      }
    )
  }
  render () {
    return (
      <>
      <form>
            <label htmlFor="FrName">
              Name:
              <input type="text" name="name" value={this.state.name} 
                 onChange={(event) => this.handleOnChangeFName(event)}
              />
            </label>
            <label htmlFor="FrValue"/>
              value:
            <input type="text" value={this.state.price} 
                  onChange={(event) => this.handleOnChangeFValue(event)}
            />
      </form>
      <input type="submit" 
                   onClick={(event) => this.handleOnFClick(event)}
            />
      </>
    )
  }
}
export default ClassForm;